


    var width_carousel = $(document).find('.carousel-item').outerWidth();


    $(".carousel-item").each(function () {
        var width_carousel = $(document).find('.carousel__wrapper').outerWidth();
        var tmp = $(this);
        tmp.attr('style', 'width: ' + width_carousel + 'px');
    });


    $(window).on('resize', function () {

        $(".carousel-item").each(function () {
            var width_carousel = $(document).find('.carousel__wrapper').outerWidth();
            var tmp = $(this);
            tmp.attr('style', 'width: ' + width_carousel + 'px');

        });
    });

    $(document).on('click', "#btn-left", function () {
        var carusel = $(this).parents('.carousel-block');
        left_carusel(carusel);
        return false;
    });
    $(document).on('click', "#btn-right", function () {
        var carusel = $(this).parents('.carousel-block');
        right_carusel(carusel);
        return false;
    });

    function left_carusel(carusel) {
        var block_width = $(carusel).find('.carousel-item').outerWidth();
        $(carusel).find(".carousel-list .carousel-item").eq(-1).clone().prependTo($(carusel).find(".carousel-list"));
        $(carusel).find(".carousel-list").css({"left": "-" + block_width + "px"});
        $(carusel).find(".carousel-list .carousel-item").eq(-1).remove();
        $(carusel).find(".carousel-list").animate({left: "0px"}, 500);
    }

    function right_carusel(carusel) {
        var block_width = $(carusel).find('.carousel-item').outerWidth();
        $(carusel).find(".carousel-list").animate({left: "-" + block_width + "px"}, 500, function () {
            $(carusel).find(".carousel-list .carousel-item").eq(0).clone().appendTo($(carusel).find(".carousel-list"));
            $(carusel).find(".carousel-list .carousel-item").eq(0).remove();
            $(carusel).find(".carousel-list").css({"left": "0px"});

        });
    }

